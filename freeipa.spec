%global alt_name ipa
%global modulename ipa

%global selinuxtype targeted
%global krb5_kdb_version 8.0

%bcond_with ipatests
%bcond_without ONLY_CLIENT

Summary:        The Identity, Policy and Audit system
Name:           freeipa
Version:        4.12.2
Release:        3%{?dist}
License:        GPL-3.0-or-later
URL:            http://www.freeipa.org/
Source0:        https://releases.pagure.org/freeipa/freeipa-%{version}.tar.gz

Patch0001:      0001-Revert-Replace-netifaces-with-ifaddr.patch

BuildRequires:  krb5-kdb-version = %{krb5_kdb_version}
BuildRequires:  krb5-kdb-devel-version = %{krb5_kdb_version}
BuildRequires:  gcc make pkgconfig pkgconf autoconf automake make libtool krb5-devel pkgconfig(krb5) 
BuildRequires:  libcurl-devel diffstat openldap-devel jansson-devel popt-devel gettext gettext-devel 
BuildRequires:  libini_config-devel cyrus-sasl-devel systemd-devel selinux-policy-devel httpd nspr-devel openssl-devel
BuildRequires:  python3-devel python3-setuptools python3-cffi python3-dns python3-ldap python3-libsss_nss_idmap
BuildRequires:  python3-netaddr python3-pyasn1 python3-pyasn1-modules python3-six python3-psutil
%if %{without ONLY_CLIENT}
BuildRequires:  389-ds-base-devel >= 2.0.3-3
BuildRequires:  libsss_idmap-devel libsss_certmap-devel libsss_nss_idmap-devel libverto-devel
BuildRequires:  samba-devel libtalloc-devel libtevent-devel libuuid-devel libpwquality-devel nodejs(abi)
BuildRequires:  libunistring-devel python3-rjsmin python3-lesscpy cracklib-dicts libcmocka-devel krb5-server
%endif


%description
IPA is an integrated solution to provide centrally managed Identity (users,
hosts, services), Authentication (SSO, 2FA), and Authorization
(host access control, SELinux user roles, services). The solution provides
features for further integration with Linux based clients (SUDO, automount)
and integration with Active Directory based infrastructures (Trusts).


%if %{without ONLY_CLIENT}
%package server
Summary:  The IPA authentication server
Requires: %{name}-server-common = %{version}-%{release}
Requires: %{name}-client = %{version}-%{release}
Requires: %{name}-common = %{version}-%{release}
Requires: python3-ipaserver = %{version}-%{release}
Requires: nss-tools krb5-pkinit-openssl cyrus-sasl-gssapi chrony httpd acl tar gzip sssd-dbus
Requires: python3-ldap python3-gssapi python3-systemd systemd-units selinux-policy policycoreutils 
Requires: python3-mod_wsgi mod_auth_gssapi mod_ssl mod_session mod_lookup_identity cracklib-dicts
Requires: fontawesome-fonts open-sans-fonts openssl softhsm p11-kit oddjob gssproxy libpwquality 
Requires: 389-ds-base >= 2.0.3-3
Requires: openldap-clients > 2.4.35-4
Requires: slapi-nis >= 0.56.4
Requires: pki-ca >= 10.10.5
Requires: pki-kra >= 10.10.5
Requires: (pki-acme >= 10.10.5 if pki-ca >= 10.10.0)
Requires: krb5-kdb-version = %{krb5_kdb_version}
Requires: %{_sysconfdir}/systemd/system
Requires(pre): systemd-units shadow-utils certmonger
Requires(pre): 389-ds-base >= 2.0.3-3
Requires(post): systemd-units selinux-policy-base krb5-server
Requires(preun): python3 systemd-units
Requires(postun): python3 systemd-units
Provides: %{alt_name}-server = %{version}
Conflicts: %{alt_name}-server
Obsoletes: %{alt_name}-server < %{version}

%description server
IPA is an integrated solution to provide centrally managed Identity (users,
hosts, services), Authentication (SSO, 2FA), and Authorization
(host access control, SELinux user roles, services). The solution provides
features for further integration with Linux based clients (SUDO, automount)
and integration with Active Directory based infrastructures (Trusts).
If you are installing an IPA server, you need to install this package.


%package -n python3-ipaserver
Summary: Python libraries used by IPA server
Requires: %{name}-server-common = %{version}-%{release}
Requires: %{name}-common = %{version}-%{release}
Requires: python3-augeas augeas-libs python3-dbus python3-dns python3-gssapi python3-kdcproxy
Requires: python3-lxmlpython3-pyasn1python3-sssdconfigpython3-psutilrpm-libs python3-urllib3
Requires: python3-ipaclient = %{version}-%{release}
Requires: python3-pki >= 10.10.5
Requires(pre): python3-ldap
BuildArch: noarch

%description -n python3-ipaserver
IPA is an integrated solution to provide centrally managed Identity (users,
hosts, services), Authentication (SSO, 2FA), and Authorization
(host access control, SELinux user roles, services). The solution provides
features for further integration with Linux based clients (SUDO, automount)
and integration with Active Directory based infrastructures (Trusts).
If you are installing an IPA server, you need to install this package.


%package server-common
Summary: Common files used by IPA server
Requires: %{name}-client-common = %{version}-%{release}
Requires: httpd systemd-units
Provides: %{alt_name}-server-common = %{version}
Conflicts: %{alt_name}-server-common
Obsoletes: %{alt_name}-server-common < %{version}
BuildArch: noarch

%description server-common
IPA is an integrated solution to provide centrally managed Identity (users,
hosts, services), Authentication (SSO, 2FA), and Authorization
(host access control, SELinux user roles, services). The solution provides
features for further integration with Linux based clients (SUDO, automount)
and integration with Active Directory based infrastructures (Trusts).
If you are installing an IPA server, you need to install this package.


%package server-dns
Summary: IPA integrated DNS server with support for automatic DNSSEC signing
Requires: %{name}-server = %{version}-%{release}
Requires: bind-dyndb-ldap bind bind-utils bind-dnssec-utils softhsm openssl-pkcs11 opendnssec
%{?systemd_requires}
Provides: %{alt_name}-server-dns = %{version}
Conflicts: %{alt_name}-server-dns
Obsoletes: %{alt_name}-server-dns < %{version}
BuildArch: noarch

%description server-dns
IPA integrated DNS server with support for automatic DNSSEC signing.
Integrated DNS server is BIND 9. OpenDNSSEC provides key management.


%package server-trust-ad
Summary: Virtual package to install packages required for Active Directory trusts
Requires: %{name}-server = %{version}-%{release}
Requires: %{name}-common = %{version}-%{release}
Requires: samba samba-winbind sssd-winbind-idmap libsss_idmap python3-samba python3-libsss_nss_idmap python3-sss
Requires(post): python3
Requires(post): %{_sbindir}/update-alternatives
Requires(postun): %{_sbindir}/update-alternatives
Requires(preun): %{_sbindir}/update-alternatives
Provides: %{alt_name}-server-trust-ad = %{version}
Conflicts: %{alt_name}-server-trust-ad
Obsoletes: %{alt_name}-server-trust-ad < %{version}

%description server-trust-ad
Cross-realm trusts with Active Directory in IPA require working Samba 4
installation. This package is provided for convenience to install all required
dependencies at once.
%endif


%package client
Summary: IPA authentication for use on clients
Requires: %{name}-client-common = %{version}-%{release}
Requires: %{name}-common = %{version}-%{release}
Requires: python3-ipaclient = %{version}-%{release}
Requires: python3-gssapi python3-ldap python3-sssdconfig cyrus-sasl-gssapi chrony krb5-client
Requires: authselect curl hostname libcurl jansson sssd-ipa sssd-idp certmonger nss-tools
Requires: bind-utils oddjob-mkhomedir libsss_autofs autofs libnfsidmap nfs-utils sssd-tools
Requires: (libsss_sudo if sudo)
Requires(post): policycoreutils
Recommends: libsss_sudo sudo
Provides: %{alt_name}-client = %{version}
Provides: %{alt_name}-admintools = %{version}
Provides: %{name}-admintools = %{version}-%{release}
Conflicts: %{alt_name}-client
Conflicts: %{alt_name}-admintools
Obsoletes: %{alt_name}-client < %{version}

%description client
IPA is an integrated solution to provide centrally managed Identity (users,
hosts, services), Authentication (SSO, 2FA), and Authorization
(host access control, SELinux user roles, services). The solution provides
features for further integration with Linux based clients (SUDO, automount)
and integration with Active Directory based infrastructures (Trusts).
If your network uses IPA for authentication, this package should be
installed on every client machine.
This package provides command-line tools for IPA administrators.


%package client-samba
Summary: Tools to configure Samba on IPA client
Requires: %{name}-client = %{version}-%{release}
Requires: python3-samba samba-client samba-winbind samba-common-tools samba sssd-winbind-idmap tdb-tools cifs-utils

%description client-samba
This package provides command-line tools to deploy Samba domain member
on the machine enrolled into a FreeIPA environment


%package client-epn
Summary: Tools to configure Expiring Password Notification in IPA
Requires: %{name}-client = %{version}-%{release}
Requires: systemd-units
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units

%description client-epn
This package provides a service to collect and send expiring password
notifications via email (SMTP).


%package -n python3-ipaclient
Summary: Python libraries used by IPA client
Requires: %{name}-client-common = %{version}-%{release}
Requires: %{name}-common = %{version}-%{release}
Requires: python3-ipalib = %{version}-%{release}
Requires: python3-augeas augeas-libs python3-dns python3-jinja2
BuildArch: noarch

%description -n python3-ipaclient
IPA is an integrated solution to provide centrally managed Identity (users,
hosts, services), Authentication (SSO, 2FA), and Authorization
(host access control, SELinux user roles, services). The solution provides
features for further integration with Linux based clients (SUDO, automount)
and integration with Active Directory based infrastructures (Trusts).
If your network uses IPA for authentication, this package should be
installed on every client machine.


%package client-common
Summary: Common files used by IPA client
Provides: %{alt_name}-client-common = %{version}
Conflicts: %{alt_name}-client-common
Obsoletes: %{alt_name}-client-common < %{version}
BuildArch: noarch

%description client-common
IPA is an integrated solution to provide centrally managed Identity (users,
hosts, services), Authentication (SSO, 2FA), and Authorization
(host access control, SELinux user roles, services). The solution provides
features for further integration with Linux based clients (SUDO, automount)
and integration with Active Directory based infrastructures (Trusts).
If your network uses IPA for authentication, this package should be
installed on every client machine.


%package python-compat
Summary: Compatiblity package for Python libraries used by IPA
Requires: %{name}-common = %{version}-%{release}
Requires: python3-ipalib = %{version}-%{release}
Provides: %{name}-python = %{version}-%{release}
Provides: %{alt_name}-python = %{version}
Provides: %{alt_name}-python-compat = %{version}
Conflicts: %{alt_name}-python-compat
Obsoletes: %{alt_name}-python-compat < %{version}
BuildArch: noarch

%description python-compat
IPA is an integrated solution to provide centrally managed Identity (users,
hosts, services), Authentication (SSO, 2FA), and Authorization
(host access control, SELinux user roles, services). The solution provides
features for further integration with Linux based clients (SUDO, automount)
and integration with Active Directory based infrastructures (Trusts).
This is a compatibility package to accommodate %{name}-python split into
python3-ipalib and %{name}-common. Packages still depending on
%{name}-python should be fixed to depend on python2-ipaclient or
%{name}-common instead.


%package -n python3-ipalib
Summary: Python3 libraries used by IPA
Provides: python3-ipapython = %{version}-%{release}
Provides: python3-ipaplatform = %{version}-%{release}
Requires: %{name}-common = %{version}-%{release}
Requires: gnupg2 keyutils python3-cffi python3-cryptography python3-dateutil python3-dbus python3-dns python3-gssapi
Requires: python3-jwcrypto python3-libipa_hbac python3-netaddr python3-netifaces python3-pyasn1 python3-pyasn1-modules
Requires: python3-pyusb python3-qrcode-core python3-requests python3-six python3-sss-murmur python3-yubico python3-setuptools
Requires: python3-systemd
Requires(pre): python3-ldap
BuildArch: noarch

%description -n python3-ipalib
IPA is an integrated solution to provide centrally managed Identity (users,
hosts, services), Authentication (SSO, 2FA), and Authorization
(host access control, SELinux user roles, services). The solution provides
features for further integration with Linux based clients (SUDO, automount)
and integration with Active Directory based infrastructures (Trusts).
If you are using IPA with Python 3, you need to install this package.


%package common
Summary: Common files used by IPA
Requires: (%{name}-selinux if selinux-policy-%{selinuxtype})
Provides: %{alt_name}-common = %{version}
Conflicts: %{alt_name}-common
Conflicts: %{alt_name}-python < %{version}
Obsoletes: %{alt_name}-common < %{version}
BuildArch: noarch

%description common
IPA is an integrated solution to provide centrally managed Identity (users,
hosts, services), Authentication (SSO, 2FA), and Authorization
(host access control, SELinux user roles, services). The solution provides
features for further integration with Linux based clients (SUDO, automount)
and integration with Active Directory based infrastructures (Trusts).
If you are using IPA, you need to install this package.


%if %{with ipatests}
%package -n python3-ipatests
Summary: IPA tests and test tools
Requires: python3-ipaclient = %{version}-%{release}
Requires: python3-ipaserver = %{version}-%{release}
Requires: iptables python3-cryptography python3-sssdconfig tar xz openssh-clients
BuildArch: noarch
AutoReqProv: no

%description -n python3-ipatests
IPA is an integrated solution to provide centrally managed Identity (users,
hosts, services), Authentication (SSO, 2FA), and Authorization
(host access control, SELinux user roles, services). The solution provides
features for further integration with Linux based clients (SUDO, automount)
and integration with Active Directory based infrastructures (Trusts).
This package contains tests that verify IPA functionality under Python 3.
%endif


%package selinux
Summary:             FreeIPA SELinux policy
Requires:            selinux-policy-%{selinuxtype}
Requires(post):      selinux-policy-%{selinuxtype}
%{?selinux_requires}
BuildArch:           noarch

%description selinux
Custom SELinux policy module for FreeIPA


%package selinux-nfast
Summary:             FreeIPA SELinux policy for nCipher nfast HSMs
BuildArch:           noarch
Requires:            selinux-policy-%{selinuxtype}
Requires(post):      selinux-policy-%{selinuxtype}
%{?selinux_requires}

%description selinux-nfast
Custom SELinux policy module for nCipher nfast HSMs

%package selinux-luna
Summary:             FreeIPA SELinux policy for Thales Luna HSMs
BuildArch:           noarch
Requires:            selinux-policy-%{selinuxtype}
Requires(post):      selinux-policy-%{selinuxtype}
%{?selinux_requires}

%description selinux-luna
Custom SELinux policy module for Thales Luna HSMs

%prep
%autosetup -n freeipa-%{version} -p1

pushd po
for i in *.po ; do
    msgattrib --translated --no-fuzzy --no-location -s $i > $i.tmp || exit 1
    mv $i.tmp $i || exit 1
done
popd



%build
autoreconf -fiv

export PYTHON=%{__python3}
%configure --with-vendor-suffix=-%{release} \
%if %{with ONLY_CLIENT}
    --disable-server \
%else
    --enable-server \
%endif
%if %{with ipatests}
    --with-ipatests \
%else
    --without-ipatests \
%endif
    --without-ipa-join-xml \
    --disable-pylint --without-jslint --disable-rpmlint

%make_build -Onone


%install
%make_install
rm -rf %{buildroot}%{python3_sitelib}/ipasphinx*

%if %{with ipatests}
mv %{buildroot}%{_bindir}/ipa-run-tests %{buildroot}%{_bindir}/ipa-run-tests-%{python3_version}
mv %{buildroot}%{_bindir}/ipa-test-config %{buildroot}%{_bindir}/ipa-test-config-%{python3_version}
mv %{buildroot}%{_bindir}/ipa-test-task %{buildroot}%{_bindir}/ipa-test-task-%{python3_version}
ln -rs %{buildroot}%{_bindir}/ipa-run-tests-%{python3_version} %{buildroot}%{_bindir}/ipa-run-tests-3
ln -rs %{buildroot}%{_bindir}/ipa-test-config-%{python3_version} %{buildroot}%{_bindir}/ipa-test-config-3
ln -rs %{buildroot}%{_bindir}/ipa-test-task-%{python3_version} %{buildroot}%{_bindir}/ipa-test-task-3
ln -frs %{buildroot}%{_bindir}/ipa-run-tests-%{python3_version} %{buildroot}%{_bindir}/ipa-run-tests
ln -frs %{buildroot}%{_bindir}/ipa-test-config-%{python3_version} %{buildroot}%{_bindir}/ipa-test-config
ln -frs %{buildroot}%{_bindir}/ipa-test-task-%{python3_version} %{buildroot}%{_bindir}/ipa-test-task
%endif

find %{buildroot} -wholename '*/site-packages/*/install_files.txt' -delete
rm -f %{buildroot}%{_datadir}/ipa/ui/images/header-logo.png
rm -f %{buildroot}%{_datadir}/ipa/ui/images/login-screen-background.jpg
rm -f %{buildroot}%{_datadir}/ipa/ui/images/login-screen-logo.png
rm -f %{buildroot}%{_datadir}/ipa/ui/images/product-name.png


%if %{without ONLY_CLIENT}
rm %{buildroot}/%{_libdir}/dirsrv/plugins/libipa_pwd_extop.la
rm %{buildroot}/%{_libdir}/dirsrv/plugins/libipa_enrollment_extop.la
rm %{buildroot}/%{_libdir}/dirsrv/plugins/libipa_winsync.la
rm %{buildroot}/%{_libdir}/dirsrv/plugins/libipa_repl_version.la
rm %{buildroot}/%{_libdir}/dirsrv/plugins/libipa_uuid.la
rm %{buildroot}/%{_libdir}/dirsrv/plugins/libipa_modrdn.la
rm %{buildroot}/%{_libdir}/dirsrv/plugins/libipa_lockout.la
rm %{buildroot}/%{_libdir}/dirsrv/plugins/libipa_cldap.la
rm %{buildroot}/%{_libdir}/dirsrv/plugins/libipa_dns.la
rm %{buildroot}/%{_libdir}/dirsrv/plugins/libipa_sidgen.la
rm %{buildroot}/%{_libdir}/dirsrv/plugins/libipa_sidgen_task.la
rm %{buildroot}/%{_libdir}/dirsrv/plugins/libipa_extdom_extop.la
rm %{buildroot}/%{_libdir}/dirsrv/plugins/libipa_range_check.la
rm %{buildroot}/%{_libdir}/dirsrv/plugins/libipa_otp_counter.la
rm %{buildroot}/%{_libdir}/dirsrv/plugins/libipa_otp_lasttoken.la
rm %{buildroot}/%{_libdir}/dirsrv/plugins/libipa_graceperiod.la
rm %{buildroot}/%{_libdir}/dirsrv/plugins/libtopology.la
rm %{buildroot}/%{_libdir}/krb5/plugins/kdb/ipadb.la
rm %{buildroot}/%{_libdir}/samba/pdb/ipasam.la
mkdir -p %{buildroot}%{_sysconfdir}/httpd/conf.d/
/bin/touch %{buildroot}%{_sysconfdir}/httpd/conf.d/ipa.conf
/bin/touch %{buildroot}%{_sysconfdir}/httpd/conf.d/ipa-kdc-proxy.conf
/bin/touch %{buildroot}%{_sysconfdir}/httpd/conf.d/ipa-pki-proxy.conf
/bin/touch %{buildroot}%{_sysconfdir}/httpd/conf.d/ipa-rewrite.conf
/bin/touch %{buildroot}%{_datadir}/ipa/html/ca.crt
/bin/touch %{buildroot}%{_datadir}/ipa/html/krb.con
/bin/touch %{buildroot}%{_datadir}/ipa/html/krb5.ini
/bin/touch %{buildroot}%{_datadir}/ipa/html/krbrealm.con

mkdir -p %{buildroot}%{_libdir}/krb5/plugins/libkrb5
touch %{buildroot}%{_libdir}/krb5/plugins/libkrb5/winbind_krb5_locator.so

mkdir -p %{buildroot}%{_sysconfdir}/cron.d
%endif

/bin/touch %{buildroot}%{_sysconfdir}/ipa/default.conf
/bin/touch %{buildroot}%{_sysconfdir}/ipa/ca.crt



%check
%make_build check VERBOSE=yes LIBDIR=%{_libdir}


%find_lang %{modulename}


%if %{without ONLY_CLIENT}
%post server
    /bin/systemctl --system daemon-reload 2>&1 || :
if [ $1 -gt 1 ] ; then
    /bin/systemctl condrestart certmonger.service 2>&1 || :
fi
/bin/systemctl reload-or-try-restart dbus
/bin/systemctl reload-or-try-restart oddjobd

%tmpfiles_create ipa.conf
%journal_catalog_update

%postun server
%journal_catalog_update


%posttrans server
%{__python3} -c "import sys; from ipalib import facts; sys.exit(0 if facts.is_ipa_configured() else 1);" > /dev/null 2>&1

if [  $? -eq 0 ]; then
    /bin/systemctl start network-online.target
    /bin/systemctl is-enabled ipa.service >/dev/null 2>&1
    if [  $? -eq 0 ]; then
        /bin/systemctl restart ipa.service >/dev/null
    fi

    /bin/systemctl is-enabled ipa-ccache-sweep.timer >/dev/null 2>&1
    if [  $? -eq 1 ]; then
        /bin/systemctl enable ipa-ccache-sweep.timer>/dev/null
    fi
fi


%preun server
if [ $1 = 0 ]; then
    /bin/systemctl --quiet stop ipa.service || :
    /bin/systemctl --quiet disable ipa.service || :
    /bin/systemctl reload-or-try-restart dbus
    /bin/systemctl reload-or-try-restart oddjobd
fi


%pre server
if [ -e /usr/sbin/ipa_kpasswd ]; then
    /bin/systemctl stop ipa_kpasswd.service >/dev/null 2>&1 || :
fi


%pre server-common
getent group kdcproxy >/dev/null || groupadd -f -r kdcproxy
getent passwd kdcproxy >/dev/null || useradd -r -g kdcproxy -s /sbin/nologin -d / -c "IPA KDC Proxy User" kdcproxy
getent group ipaapi >/dev/null || groupadd -f -r ipaapi
getent passwd ipaapi >/dev/null || useradd -r -g ipaapi -s /sbin/nologin -d / -c "IPA Framework User" ipaapi
id -Gn apache | grep '\bipaapi\b' >/dev/null || usermod apache -a -G ipaapi


%post server-dns
%systemd_post ipa-dnskeysyncd.service ipa-ods-exporter.socket ipa-ods-exporter.service

%preun server-dns
%systemd_preun ipa-dnskeysyncd.service ipa-ods-exporter.socket ipa-ods-exporter.service

%postun server-dns
%systemd_postun ipa-dnskeysyncd.service ipa-ods-exporter.socket ipa-ods-exporter.service


%postun server-trust-ad
if [ "$1" -ge "1" ]; then
    if [ "`readlink %{_sysconfdir}/alternatives/winbind_krb5_locator.so`" == "/dev/null" ]; then
        %{_sbindir}/alternatives --set winbind_krb5_locator.so /dev/null
    fi
fi


%post server-trust-ad
%{_sbindir}/update-alternatives --install %{_libdir}/krb5/plugins/libkrb5/winbind_krb5_locator.so \
        winbind_krb5_locator.so /dev/null 90
/bin/systemctl reload-or-try-restart dbus
/bin/systemctl reload-or-try-restart oddjobd


%posttrans server-trust-ad
%{__python3} -c "import sys; from ipalib import facts; sys.exit(0 if facts.is_ipa_configured() else 1);" > /dev/null 2>&1
if [  $? -eq 0 ]; then
    /bin/systemctl try-restart httpd.service >/dev/null 2>&1 || :
fi


%preun server-trust-ad
if [ $1 -eq 0 ]; then
    %{_sbindir}/update-alternatives --remove winbind_krb5_locator.so /dev/null
    /bin/systemctl reload-or-try-restart dbus
    /bin/systemctl reload-or-try-restart oddjobd
fi
%endif

%preun client-epn
%systemd_preun ipa-epn.service
%systemd_preun ipa-epn.timer

%postun client-epn
%systemd_postun ipa-epn.service
%systemd_postun ipa-epn.timer

%post client-epn
%systemd_post ipa-epn.service
%systemd_post ipa-epn.timer

%post client
if [ $1 -gt 1 ] ; then
    restore=0
    test -f '/var/lib/ipa-client/sysrestore/sysrestore.index' && restore=$(wc -l '/var/lib/ipa-client/sysrestore/sysrestore.index' | awk '{print $1}')

    if [ -f '/etc/sssd/sssd.conf' -a $restore -ge 2 ]; then
        if ! grep -E -q '/var/lib/sss/pubconf/krb5.include.d/' /etc/krb5.conf  2>/dev/null ; then
            echo "includedir /var/lib/sss/pubconf/krb5.include.d/" > /etc/krb5.conf.ipanew
            cat /etc/krb5.conf >> /etc/krb5.conf.ipanew
            mv -Z /etc/krb5.conf.ipanew /etc/krb5.conf
        fi
    fi

    if [ $restore -ge 2 ]; then
        if grep -E -q '\s*pkinit_anchors = FILE:/etc/ipa/ca.crt$' /etc/krb5.conf 2>/dev/null; then
            sed -E 's|(\s*)pkinit_anchors = FILE:/etc/ipa/ca.crt$|\1pkinit_anchors = FILE:/var/lib/ipa-client/pki/kdc-ca-bundle.pem\n\1pkinit_pool = FILE:/var/lib/ipa-client/pki/ca-bundle.pem|' /etc/krb5.conf >/etc/krb5.conf.ipanew
            mv -Z /etc/krb5.conf.ipanew /etc/krb5.conf
            cp /etc/ipa/ca.crt /var/lib/ipa-client/pki/kdc-ca-bundle.pem
            cp /etc/ipa/ca.crt /var/lib/ipa-client/pki/ca-bundle.pem
        fi

        %{__python3} -c 'from ipaclient.install.client import configure_krb5_snippet; configure_krb5_snippet()' >>/var/log/ipaupgrade.log 2>&1
        %{__python3} -c 'from ipaclient.install.client import update_ipa_nssdb; update_ipa_nssdb()' >>/var/log/ipaupgrade.log 2>&1
        chmod 0600 /var/log/ipaupgrade.log
        SSH_CLIENT_SYSTEM_CONF="/etc/ssh/ssh_config"
        if [ -f "$SSH_CLIENT_SYSTEM_CONF" ]; then
            if grep -E -q '^HostKeyAlgorithms ssh-rsa,ssh-dss' $SSH_CLIENT_SYSTEM_CONF 2>/dev/null; then
                sed -E --in-place=.orig 's/^(HostKeyAlgorithms ssh-rsa,ssh-dss)$/# disabled by ipa-client update\n# \1/' "$SSH_CLIENT_SYSTEM_CONF"
            fi
            # https://pagure.io/freeipa/issue/9536
            # replace sss_ssh_knownhostsproxy with sss_ssh_knownhosts
            if [ -f '/usr/bin/sss_ssh_knownhosts' ]; then
                if grep -E -q 'Include' $SSH_CLIENT_SYSTEM_CONF  2>/dev/null ; then
                    SSH_CLIENT_SYSTEM_CONF="/etc/ssh/ssh_config.d/04-ipa.conf"
                fi
                sed -E --in-place=.orig 's/^(GlobalKnownHostsFile \/var\/lib\/sss\/pubconf\/known_hosts)$/# disabled by ipa-client update\n# \1/' $SSH_CLIENT_SYSTEM_CONF
                sed -E --in-place=.orig 's/(ProxyCommand \/usr\/bin\/sss_ssh_knownhostsproxy -p \%p \%h)/# replaced by ipa-client update\n    KnownHostsCommand \/usr\/bin\/sss_ssh_knownhosts \%H/' $SSH_CLIENT_SYSTEM_CONF
            fi
        fi
    fi
fi


%pre selinux
%selinux_relabel_pre -s %{selinuxtype}

%post selinux
semodule -d ipa_custodia &> /dev/null || true;
%selinux_modules_install -s %{selinuxtype} %{_datadir}/selinux/packages/%{selinuxtype}/%{modulename}.pp.bz2

%post selinux-nfast
%selinux_modules_install -s %{selinuxtype} %{_datadir}/selinux/packages/%{selinuxtype}/%{modulename}-nfast.pp.bz2

%post selinux-luna
%selinux_modules_install -s %{selinuxtype} %{_datadir}/selinux/packages/%{selinuxtype}/%{modulename}-luna.pp.bz2


%postun selinux
if [ $1 -eq 0 ]; then
    %selinux_modules_uninstall -s %{selinuxtype} %{modulename}
    semodule -e ipa_custodia &> /dev/null || true;
fi

%postun selinux-nfast
if [ $1 -eq 0 ]; then
    %selinux_modules_uninstall -s %{selinuxtype} %{modulename}-nfast
fi

%postun selinux-luna
if [ $1 -eq 0 ]; then
    %selinux_modules_uninstall -s %{selinuxtype} %{modulename}-luna
fi

%posttrans selinux
%selinux_relabel_post -s %{selinuxtype}


%triggerin client -- sssd-common < 2.10
# Has the client been configured?
restore=0
test -f '/var/lib/ipa-client/sysrestore/sysrestore.index' && restore=$(wc -l '/var/lib/ipa-client/sysrestore/sysrestore.index' | awk '{print $1}')

if [ -f '/etc/ssh/sshd_config' -a $restore -ge 2 ]; then
    SSH_CLIENT_SYSTEM_CONF="/etc/ssh/ssh_config"
    if [ -f "$SSH_CLIENT_SYSTEM_CONF" ]; then
        # https://pagure.io/freeipa/issue/9536
        # downgrade sss_ssh_knownhosts with sss_ssh_knownhostsproxy
        if [ -f '/usr/bin/sss_ssh_knownhosts' ]; then
            if grep -E -q 'Include' $SSH_CLIENT_SYSTEM_CONF  2>/dev/null ; then
                SSH_CLIENT_SYSTEM_CONF="/etc/ssh/ssh_config.d/04-ipa.conf"
            fi
            GLOBALKNOWNHOSTFILE="GlobalKnownHostsFile /var/lib/sss/pubconf/known_hosts/"
            grep -qF '$GLOBALKNOWNHOSTFILE' $SSH_CLIENT_SYSTEM_CONF
            if [ $? -ne 0 ]; then
                sed -E --in-place=.orig '/(# IPA-related configuration changes to ssh_config)/a # added by ipa-client update\n'"$GLOBALKNOWNHOSTFILE"'' $SSH_CLIENT_SYSTEM_CONF
            fi
            sed -E --in-place=.orig 's/(KnownHostsCommand \/usr\/bin\/sss_ssh_knownhosts \%H)/ProxyCommand \/usr\/bin\/sss_ssh_knownhostsproxy -p \%p \%h/' $SSH_CLIENT_SYSTEM_CONF
        fi
    fi
fi

%triggerin client -- sssd-common >= 2.10
# Has the client been configured?
restore=0
test -f '/var/lib/ipa-client/sysrestore/sysrestore.index' && restore=$(wc -l '/var/lib/ipa-client/sysrestore/sysrestore.index' | awk '{print $1}')

if [ -f '/etc/ssh/sshd_config' -a $restore -ge 2 ]; then
    SSH_CLIENT_SYSTEM_CONF="/etc/ssh/ssh_config"
    if [ -f "$SSH_CLIENT_SYSTEM_CONF" ]; then
        # https://pagure.io/freeipa/issue/9536
        # upgrade sss_ssh_knownhostsproxy with sss_ssh_knownhosts
        if [ -f '/usr/bin/sss_ssh_knownhosts' ]; then
            if grep -E -q 'Include' $SSH_CLIENT_SYSTEM_CONF  2>/dev/null ; then
                SSH_CLIENT_SYSTEM_CONF="/etc/ssh/ssh_config.d/04-ipa.conf"
            fi
            sed -E --in-place=.orig 's/^(GlobalKnownHostsFile \/var\/lib\/sss\/pubconf\/known_hosts)$/# disabled by ipa-client update\n# \1/' $SSH_CLIENT_SYSTEM_CONF
            sed -E --in-place=.orig 's/(ProxyCommand \/usr\/bin\/sss_ssh_knownhostsproxy -p \%p \%h)/# replaced by ipa-client update\n    KnownHostsCommand \/usr\/bin\/sss_ssh_knownhosts \%H/' $SSH_CLIENT_SYSTEM_CONF
        fi
    fi
fi


%triggerin client -- openssh-server < 8.2
restore=0
test -f '/var/lib/ipa-client/sysrestore/sysrestore.index' && restore=$(wc -l '/var/lib/ipa-client/sysrestore/sysrestore.index' | awk '{print $1}')

if [ -f '/etc/ssh/sshd_config' -a $restore -ge 2 ]; then
    if grep -E -q '^(AuthorizedKeysCommand /usr/bin/sss_ssh_authorizedkeys|PubKeyAgent /usr/bin/sss_ssh_authorizedkeys %u)$' /etc/ssh/sshd_config 2>/dev/null; then
        sed -r '
            /^(AuthorizedKeysCommand(User|RunAs)|PubKeyAgentRunAs)[ \t]/ d
        ' /etc/ssh/sshd_config >/etc/ssh/sshd_config.ipanew

        if /usr/sbin/sshd -t -f /dev/null -o 'AuthorizedKeysCommand=/usr/bin/sss_ssh_authorizedkeys' -o 'AuthorizedKeysCommandUser=nobody' 2>/dev/null; then
            sed -ri '
                s/^PubKeyAgent (.+) %u$/AuthorizedKeysCommand \1/
                s/^AuthorizedKeysCommand .*$/\0\nAuthorizedKeysCommandUser nobody/
            ' /etc/ssh/sshd_config.ipanew
        elif /usr/sbin/sshd -t -f /dev/null -o 'AuthorizedKeysCommand=/usr/bin/sss_ssh_authorizedkeys' -o 'AuthorizedKeysCommandRunAs=nobody' 2>/dev/null; then
            sed -ri '
                s/^PubKeyAgent (.+) %u$/AuthorizedKeysCommand \1/
                s/^AuthorizedKeysCommand .*$/\0\nAuthorizedKeysCommandRunAs nobody/
            ' /etc/ssh/sshd_config.ipanew
        elif /usr/sbin/sshd -t -f /dev/null -o 'PubKeyAgent=/usr/bin/sss_ssh_authorizedkeys %u' -o 'PubKeyAgentRunAs=nobody' 2>/dev/null; then
            sed -ri '
                s/^AuthorizedKeysCommand (.+)$/PubKeyAgent \1 %u/
                s/^PubKeyAgent .*$/\0\nPubKeyAgentRunAs nobody/
            ' /etc/ssh/sshd_config.ipanew
        fi

        mv -Z /etc/ssh/sshd_config.ipanew /etc/ssh/sshd_config
        chmod 600 /etc/ssh/sshd_config

        /bin/systemctl condrestart sshd.service 2>&1 || :
    fi
fi


%triggerin client -- openssh-server >= 8.2
restore=0
test -f '/var/lib/ipa-client/sysrestore/sysrestore.index' && restore=$(wc -l '/var/lib/ipa-client/sysrestore/sysrestore.index' | awk '{print $1}')

if [ -f '/etc/ssh/sshd_config' -a $restore -ge 2 ]; then
    if [ ! -f '/etc/ssh/sshd_config.d/04-ipa.conf' ]; then
        grep -E '^(PubkeyAuthentication|KerberosAuthentication|GSSAPIAuthentication|UsePAM|ChallengeResponseAuthentication|AuthorizedKeysCommand|AuthorizedKeysCommandUser)' /etc/ssh/sshd_config 2>/dev/null > /etc/ssh/sshd_config.d/04-ipa.conf
        sed -ri '
            /^(PubkeyAuthentication|KerberosAuthentication|GSSAPIAuthentication|UsePAM|ChallengeResponseAuthentication|AuthorizedKeysCommand|AuthorizedKeysCommandUser)[ \t]/ d
        ' /etc/ssh/sshd_config

        /bin/systemctl condrestart sshd.service 2>&1 || :
    fi
    if [ -f '/etc/ssh/sshd_config.d/04-ipa.conf' ]; then
        if ! grep -E -q  '^\s*Include\s*/etc/ssh/sshd_config.d/\*\.conf' /etc/ssh/sshd_config 2> /dev/null ; then
            if ! grep -E -q '^\s*Include\s*/etc/ssh/sshd_config.d/04-ipa\.conf' /etc/ssh/sshd_config 2> /dev/null ; then
                echo "Include /etc/ssh/sshd_config.d/04-ipa.conf" > /etc/ssh/sshd_config.ipanew
                cat /etc/ssh/sshd_config >> /etc/ssh/sshd_config.ipanew
                mv -fZ --backup=existing --suffix .ipaold /etc/ssh/sshd_config.ipanew /etc/ssh/sshd_config
            fi
        fi
    fi
fi


%if %{without ONLY_CLIENT}
%files server
%license COPYING
%doc README.md Contributors.txt
%{_sbindir}/ipa-backup
%{_sbindir}/ipa-restore
%{_sbindir}/ipa-ca-install
%{_sbindir}/ipa-kra-install
%{_sbindir}/ipa-server-install
%{_sbindir}/ipa-replica-conncheck
%{_sbindir}/ipa-replica-install
%{_sbindir}/ipa-replica-manage
%{_sbindir}/ipa-csreplica-manage
%{_sbindir}/ipa-server-certinstall
%{_sbindir}/ipa-server-upgrade
%{_sbindir}/ipa-ldap-updater
%{_sbindir}/ipa-otptoken-import
%{_sbindir}/ipa-compat-manage
%{_sbindir}/ipa-managed-entries
%{_sbindir}/ipactl
%{_sbindir}/ipa-advise
%{_sbindir}/ipa-cacert-manage
%{_sbindir}/ipa-winsync-migrate
%{_sbindir}/ipa-pkinit-manage
%{_sbindir}/ipa-crlgen-manage
%{_sbindir}/ipa-cert-fix
%{_sbindir}/ipa-idrange-fix
%{_sbindir}/ipa-acme-manage
%{_libexecdir}/certmonger/dogtag-ipa-ca-renew-agent-submit
%{_libexecdir}/certmonger/ipa-server-guard
%dir %{_libexecdir}/ipa
%{_libexecdir}/ipa/ipa-ccache-sweeper
%{_libexecdir}/ipa/ipa-custodia
%{_libexecdir}/ipa/ipa-custodia-check
%{_libexecdir}/ipa/ipa-httpd-kdcproxy
%{_libexecdir}/ipa/ipa-httpd-pwdreader
%{_libexecdir}/ipa/ipa-pki-retrieve-key
%{_libexecdir}/ipa/ipa-pki-wait-running
%{_libexecdir}/ipa/ipa-otpd
%{_libexecdir}/ipa/ipa-print-pac
%{_libexecdir}/ipa/ipa-subids
%dir %{_libexecdir}/ipa/custodia
%attr(755,root,root) %{_libexecdir}/ipa/custodia/ipa-custodia-dmldap
%attr(755,root,root) %{_libexecdir}/ipa/custodia/ipa-custodia-pki-tomcat
%attr(755,root,root) %{_libexecdir}/ipa/custodia/ipa-custodia-pki-tomcat-wrapped
%attr(755,root,root) %{_libexecdir}/ipa/custodia/ipa-custodia-ra-agent
%dir %{_libexecdir}/ipa/oddjob
%attr(0755,root,root) %{_libexecdir}/ipa/oddjob/org.freeipa.server.conncheck
%attr(0755,root,root) %{_libexecdir}/ipa/oddjob/org.freeipa.server.trust-enable-agent
%attr(0755,root,root) %{_libexecdir}/ipa/oddjob/org.freeipa.server.config-enable-sid
%config(noreplace) %{_sysconfdir}/dbus-1/system.d/org.freeipa.server.conf
%config(noreplace) %{_sysconfdir}/oddjobd.conf.d/ipa-server.conf
%dir %{_libexecdir}/ipa/certmonger
%attr(755,root,root) %{_libexecdir}/ipa/certmonger/*
%attr(644,root,root) %{_unitdir}/ipa.service
%attr(644,root,root) %{_unitdir}/ipa-otpd.socket
%attr(644,root,root) %{_unitdir}/ipa-otpd@.service
%attr(644,root,root) %{_unitdir}/ipa-ccache-sweep.service
%attr(644,root,root) %{_unitdir}/ipa-ccache-sweep.timer
%attr(644,root,root) %{_journalcatalogdir}/ipa.catalog
%attr(755,root,root) %{_libdir}/dirsrv/plugins/libipa_pwd_extop.so
%attr(755,root,root) %{_libdir}/dirsrv/plugins/libipa_enrollment_extop.so
%attr(755,root,root) %{_libdir}/dirsrv/plugins/libipa_winsync.so
%attr(755,root,root) %{_libdir}/dirsrv/plugins/libipa_repl_version.so
%attr(755,root,root) %{_libdir}/dirsrv/plugins/libipa_uuid.so
%attr(755,root,root) %{_libdir}/dirsrv/plugins/libipa_modrdn.so
%attr(755,root,root) %{_libdir}/dirsrv/plugins/libipa_lockout.so
%attr(755,root,root) %{_libdir}/dirsrv/plugins/libipa_dns.so
%attr(755,root,root) %{_libdir}/dirsrv/plugins/libipa_range_check.so
%attr(755,root,root) %{_libdir}/dirsrv/plugins/libipa_otp_counter.so
%attr(755,root,root) %{_libdir}/dirsrv/plugins/libipa_otp_lasttoken.so
%attr(755,root,root) %{_libdir}/dirsrv/plugins/libtopology.so
%attr(755,root,root) %{_libdir}/dirsrv/plugins/libipa_sidgen.so
%attr(755,root,root) %{_libdir}/dirsrv/plugins/libipa_sidgen_task.so
%attr(755,root,root) %{_libdir}/dirsrv/plugins/libipa_extdom_extop.so
%attr(755,root,root) %{_libdir}/dirsrv/plugins/libipa_graceperiod.so
%attr(755,root,root) %{_libdir}/krb5/plugins/kdb/ipadb.so
%{_mandir}/man1/ipa-replica-conncheck.1*
%{_mandir}/man1/ipa-replica-install.1*
%{_mandir}/man1/ipa-replica-manage.1*
%{_mandir}/man1/ipa-csreplica-manage.1*
%{_mandir}/man1/ipa-server-certinstall.1*
%{_mandir}/man1/ipa-server-install.1*
%{_mandir}/man1/ipa-server-upgrade.1*
%{_mandir}/man1/ipa-ca-install.1*
%{_mandir}/man1/ipa-kra-install.1*
%{_mandir}/man1/ipa-compat-manage.1*
%{_mandir}/man1/ipa-managed-entries.1*
%{_mandir}/man1/ipa-ldap-updater.1*
%{_mandir}/man8/ipactl.8*
%{_mandir}/man1/ipa-backup.1*
%{_mandir}/man1/ipa-restore.1*
%{_mandir}/man1/ipa-advise.1*
%{_mandir}/man1/ipa-otptoken-import.1*
%{_mandir}/man1/ipa-cacert-manage.1*
%{_mandir}/man1/ipa-winsync-migrate.1*
%{_mandir}/man1/ipa-pkinit-manage.1*
%{_mandir}/man1/ipa-crlgen-manage.1*
%{_mandir}/man1/ipa-cert-fix.1*
%{_mandir}/man1/ipa-idrange-fix.1*
%{_mandir}/man1/ipa-acme-manage.1*


%files -n python3-ipaserver
%license COPYING
%doc README.md Contributors.txt
%{python3_sitelib}/ipaserver
%{python3_sitelib}/ipaserver-*.egg-info


%files server-common
%license COPYING
%doc README.md Contributors.txt
%ghost %verify(not owner group) %dir %{_sharedstatedir}/kdcproxy
%dir %attr(0755,root,root) %{_sysconfdir}/ipa/kdcproxy
%config(noreplace) %{_sysconfdir}/ipa/kdcproxy/kdcproxy.conf
%{_tmpfilesdir}/ipa.conf
%attr(644,root,root) %{_unitdir}/ipa-custodia.service
%ghost %attr(644,root,root) %{_sysconfdir}/systemd/system/httpd.d/ipa.conf
%{_datadir}/ipa/wsgi.py*
%{_datadir}/ipa/kdcproxy.wsgi
%{_datadir}/ipa/ipaca*.ini
%{_datadir}/ipa/*.ldif
%exclude %{_datadir}/ipa/ipa-cldap-conf.ldif
%{_datadir}/ipa/*.uldif
%{_datadir}/ipa/*.template
%dir %{_datadir}/ipa/advise
%dir %{_datadir}/ipa/advise/legacy
%{_datadir}/ipa/advise/legacy/*.template
%dir %{_datadir}/ipa/profiles
%{_datadir}/ipa/profiles/README
%{_datadir}/ipa/profiles/*.cfg
%dir %{_datadir}/ipa/html
%{_datadir}/ipa/html/ssbrowser.html
%{_datadir}/ipa/html/unauthorized.html
%dir %{_datadir}/ipa/migration
%{_datadir}/ipa/migration/index.html
%{_datadir}/ipa/migration/migration.py*
%dir %{_datadir}/ipa/ui
%{_datadir}/ipa/ui/index.html
%{_datadir}/ipa/ui/reset_password.html
%{_datadir}/ipa/ui/sync_otp.html
%{_datadir}/ipa/ui/*.ico
%{_datadir}/ipa/ui/*.css
%dir %{_datadir}/ipa/ui/css
%{_datadir}/ipa/ui/css/*.css
%dir %{_datadir}/ipa/ui/js
%dir %{_datadir}/ipa/ui/js/dojo
%{_datadir}/ipa/ui/js/dojo/dojo.js
%dir %{_datadir}/ipa/ui/js/libs
%{_datadir}/ipa/ui/js/libs/*.js
%dir %{_datadir}/ipa/ui/js/freeipa
%{_datadir}/ipa/ui/js/freeipa/app.js
%{_datadir}/ipa/ui/js/freeipa/core.js
%dir %{_datadir}/ipa/ui/js/plugins
%dir %{_datadir}/ipa/ui/images
%{_datadir}/ipa/ui/images/facet-*.png
%dir %{_datadir}/ipa/wsgi
%{_datadir}/ipa/wsgi/plugins.py*
%dir %{_sysconfdir}/ipa
%dir %{_sysconfdir}/ipa/html
%config(noreplace) %{_sysconfdir}/ipa/html/ssbrowser.html
%config(noreplace) %{_sysconfdir}/ipa/html/unauthorized.html
%ghost %attr(0644,root,root) %config(noreplace) %{_sysconfdir}/httpd/conf.d/ipa-rewrite.conf
%ghost %attr(0644,root,root) %config(noreplace) %{_sysconfdir}/httpd/conf.d/ipa.conf
%ghost %attr(0644,root,root) %config(noreplace) %{_sysconfdir}/httpd/conf.d/ipa-kdc-proxy.conf
%ghost %attr(0640,root,root) %config(noreplace) %{_sysconfdir}/httpd/conf.d/ipa-pki-proxy.conf
%ghost %attr(0644,root,root) %config(noreplace) %{_sysconfdir}/ipa/kdcproxy/ipa-kdc-proxy.conf
%ghost %attr(0644,root,root) %config(noreplace) %{_datadir}/ipa/html/ca.crt
%ghost %attr(0640,root,named) %config(noreplace) %{_sysconfdir}/named/ipa-ext.conf
%ghost %attr(0640,root,named) %config(noreplace) %{_sysconfdir}/named/ipa-options-ext.conf
%ghost %attr(0644,root,root) %{_datadir}/ipa/html/krb.con
%ghost %attr(0644,root,root) %{_datadir}/ipa/html/krb5.ini
%ghost %attr(0644,root,root) %{_datadir}/ipa/html/krbrealm.con
%dir %{_datadir}/ipa/updates/
%{_datadir}/ipa/updates/*
%dir %{_localstatedir}/lib/ipa
%attr(700,root,root) %dir %{_localstatedir}/lib/ipa/backup
%attr(700,root,root) %dir %{_localstatedir}/lib/ipa/gssproxy
%attr(711,root,root) %dir %{_localstatedir}/lib/ipa/sysrestore
%attr(700,root,root) %dir %{_localstatedir}/lib/ipa/sysupgrade
%attr(755,root,root) %dir %{_localstatedir}/lib/ipa/pki-ca
%attr(755,root,root) %dir %{_localstatedir}/lib/ipa/certs
%attr(700,root,root) %dir %{_localstatedir}/lib/ipa/private
%attr(700,root,root) %dir %{_localstatedir}/lib/ipa/passwds
%ghost %attr(775,root,pkiuser) %{_localstatedir}/lib/ipa/pki-ca/publish
%ghost %attr(770,named,named) %{_localstatedir}/named/dyndb-ldap/ipa
%dir %attr(0700,root,root) %{_sysconfdir}/ipa/custodia
%dir %{_datadir}/ipa/schema.d
%attr(0644,root,root) %{_datadir}/ipa/schema.d/README
%attr(0644,root,root) %{_datadir}/ipa/gssapi.login
%{_datadir}/ipa/ipakrb5.aug

%files server-dns
%license COPYING
%doc README.md Contributors.txt
%config(noreplace) %{_sysconfdir}/sysconfig/ipa-dnskeysyncd
%config(noreplace) %{_sysconfdir}/sysconfig/ipa-ods-exporter
%dir %attr(0755,root,root) %{_sysconfdir}/ipa/dnssec
%{_libexecdir}/ipa/ipa-dnskeysyncd
%{_libexecdir}/ipa/ipa-dnskeysync-replica
%{_libexecdir}/ipa/ipa-ods-exporter
%{_sbindir}/ipa-dns-install
%{_mandir}/man1/ipa-dns-install.1*
%attr(644,root,root) %{_unitdir}/ipa-dnskeysyncd.service
%attr(644,root,root) %{_unitdir}/ipa-ods-exporter.socket
%attr(644,root,root) %{_unitdir}/ipa-ods-exporter.service


%files server-trust-ad
%license COPYING
%doc README.md Contributors.txt
%{_sbindir}/ipa-adtrust-install
%{_datadir}/ipa/smb.conf.empty
%attr(755,root,root) %{_libdir}/samba/pdb/ipasam.so
%attr(755,root,root) %{_libdir}/dirsrv/plugins/libipa_cldap.so
%{_datadir}/ipa/ipa-cldap-conf.ldif
%{_mandir}/man1/ipa-adtrust-install.1*
%ghost %{_libdir}/krb5/plugins/libkrb5/winbind_krb5_locator.so
%{_sysconfdir}/dbus-1/system.d/oddjob-ipa-trust.conf
%{_sysconfdir}/oddjobd.conf.d/oddjobd-ipa-trust.conf
%attr(755,root,root) %{_libexecdir}/ipa/oddjob/com.redhat.idm.trust-fetch-domains
%endif


%files client
%license COPYING
%doc README.md Contributors.txt
%{_sbindir}/ipa-client-install
%{_sbindir}/ipa-client-automount
%{_sbindir}/ipa-certupdate
%{_sbindir}/ipa-getkeytab
%{_sbindir}/ipa-rmkeytab
%{_sbindir}/ipa-join
%{_bindir}/ipa
%config %{_sysconfdir}/bash_completion.d
%config %{_sysconfdir}/sysconfig/certmonger
%{_mandir}/man1/ipa.1*
%{_mandir}/man1/ipa-getkeytab.1*
%{_mandir}/man1/ipa-rmkeytab.1*
%{_mandir}/man1/ipa-client-install.1*
%{_mandir}/man1/ipa-client-automount.1*
%{_mandir}/man1/ipa-certupdate.1*
%{_mandir}/man1/ipa-join.1*
%dir %{_libexecdir}/ipa/acme
%{_libexecdir}/ipa/acme/certbot-dns-ipa


%files client-samba
%license COPYING
%doc README.md Contributors.txt
%{_sbindir}/ipa-client-samba
%{_mandir}/man1/ipa-client-samba.1*


%files client-epn
%license COPYING
%doc README.md Contributors.txt
%dir %{_sysconfdir}/ipa/epn
%{_sbindir}/ipa-epn
%{_mandir}/man1/ipa-epn.1*
%{_mandir}/man5/epn.conf.5*
%attr(644,root,root) %{_unitdir}/ipa-epn.service
%attr(644,root,root) %{_unitdir}/ipa-epn.timer
%attr(600,root,root) %config(noreplace) %{_sysconfdir}/ipa/epn.conf
%attr(644,root,root) %config(noreplace) %{_sysconfdir}/ipa/epn/expire_msg.template


%files -n python3-ipaclient
%license COPYING
%doc README.md Contributors.txt
%dir %{python3_sitelib}/ipaclient
%{python3_sitelib}/ipaclient/*.py
%{python3_sitelib}/ipaclient/__pycache__/*.py*
%dir %{python3_sitelib}/ipaclient/install
%{python3_sitelib}/ipaclient/install/*.py
%{python3_sitelib}/ipaclient/install/__pycache__/*.py*
%dir %{python3_sitelib}/ipaclient/plugins
%{python3_sitelib}/ipaclient/plugins/*.py
%{python3_sitelib}/ipaclient/plugins/__pycache__/*.py*
%dir %{python3_sitelib}/ipaclient/remote_plugins
%{python3_sitelib}/ipaclient/remote_plugins/*.py
%{python3_sitelib}/ipaclient/remote_plugins/__pycache__/*.py*
%dir %{python3_sitelib}/ipaclient/remote_plugins/2_*
%{python3_sitelib}/ipaclient/remote_plugins/2_*/*.py
%{python3_sitelib}/ipaclient/remote_plugins/2_*/__pycache__/*.py*
%{python3_sitelib}/ipaclient-*.egg-info


%files client-common
%license COPYING
%doc README.md Contributors.txt
%dir %attr(0755,root,root) %{_sysconfdir}/ipa/
%ghost %attr(0644,root,root) %config(noreplace) %{_sysconfdir}/ipa/default.conf
%ghost %attr(0644,root,root) %config(noreplace) %{_sysconfdir}/ipa/ca.crt
%dir %attr(0755,root,root) %{_sysconfdir}/ipa/nssdb
%ghost %attr(644,root,root) %config(noreplace) %{_sysconfdir}/ipa/nssdb/cert8.db
%ghost %attr(644,root,root) %config(noreplace) %{_sysconfdir}/ipa/nssdb/key3.db
%ghost %attr(644,root,root) %config(noreplace) %{_sysconfdir}/ipa/nssdb/secmod.db
%ghost %attr(644,root,root) %config(noreplace) %{_sysconfdir}/ipa/nssdb/cert9.db
%ghost %attr(644,root,root) %config(noreplace) %{_sysconfdir}/ipa/nssdb/key4.db
%ghost %attr(644,root,root) %config(noreplace) %{_sysconfdir}/ipa/nssdb/pkcs11.txt
%ghost %attr(600,root,root) %config(noreplace) %{_sysconfdir}/ipa/nssdb/pwdfile.txt
%ghost %attr(644,root,root) %config(noreplace) %{_sysconfdir}/pki/ca-trust/source/ipa.p11-kit
%dir %{_localstatedir}/lib/ipa-client
%dir %{_localstatedir}/lib/ipa-client/pki
%dir %{_localstatedir}/lib/ipa-client/sysrestore
%{_mandir}/man5/default.conf.5*
%dir %{_datadir}/ipa/client
%{_datadir}/ipa/client/*.template


%files python-compat
%license COPYING
%doc README.md Contributors.txt


%files common -f %{modulename}.lang
%license COPYING
%doc README.md Contributors.txt
%dir %{_datadir}/ipa
%dir %{_libexecdir}/ipa


%files -n python3-ipalib
%license COPYING
%doc README.md Contributors.txt
%{python3_sitelib}/ipapython/
%{python3_sitelib}/ipalib/
%{python3_sitelib}/ipaplatform/
%{python3_sitelib}/ipapython-*.egg-info
%{python3_sitelib}/ipalib-*.egg-info
%{python3_sitelib}/ipaplatform-*.egg-info


%if %{with ipatests}
%files -n python3-ipatests
%license COPYING
%doc README.md Contributors.txt
%{python3_sitelib}/ipatests
%{python3_sitelib}/ipatests-*.egg-info
%{_bindir}/ipa-run-tests-3
%{_bindir}/ipa-test-config-3
%{_bindir}/ipa-test-task-3
%{_bindir}/ipa-run-tests-%{python3_version}
%{_bindir}/ipa-test-config-%{python3_version}
%{_bindir}/ipa-test-task-%{python3_version}
%{_bindir}/ipa-run-tests
%{_bindir}/ipa-test-config
%{_bindir}/ipa-test-task
%{_mandir}/man1/ipa-run-tests.1*
%{_mandir}/man1/ipa-test-config.1*
%{_mandir}/man1/ipa-test-task.1*
%endif


%files selinux
%{_datadir}/selinux/packages/%{selinuxtype}/%{modulename}.pp.*
%ghost %verify(not md5 size mode mtime) %{_sharedstatedir}/selinux/%{selinuxtype}/active/modules/200/%{modulename}


%files selinux-nfast
%{_datadir}/selinux/packages/%{selinuxtype}/%{modulename}-nfast.pp.*
%ghost %verify(not md5 size mode mtime) %{_sharedstatedir}/selinux/%{selinuxtype}/active/modules/200/%{modulename}-nfast


%files selinux-luna
%{_datadir}/selinux/packages/%{selinuxtype}/%{modulename}-luna.pp.*
%ghost %verify(not md5 size mode mtime) %{_sharedstatedir}/selinux/%{selinuxtype}/active/modules/200/%{modulename}-luna


%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.12.2-3
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Wed Sep 18 2024 Rebuild Robot <rebot@opencloudos.org> - 4.12.2-2
- [Type] security
- [DESC] Rebuilt for krb5

* Wed Sep 11 2024 Upgrade Robot <upbot@opencloudos.tech> - 4.12.2-1
- Upgrade to version 4.12.2 (Fix CVE-2024-2698, CVE-2024-3183)
- Revert "Replace netifaces with ifaddr"

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.11.1-4
- Rebuilt for loongarch release

* Tue May 14 2024 Miaojun Dong <zoedong@tencent.com> - 4.11.1-3
- [type] security
- [DESC] Fix CVE-2024-1481

* Tue Mar 19 2024 Rebuild Robot <rebot@opencloudos.org> - 4.11.1-2
- Rebuilt for jansson

* Tue Jan 30 2024 Miaojun Dong <zoedong@tencent.com> - 4.11.1-1
- Upgrade to version 4.11.1 (Fix CVE-2023-5455)
- Update opencloudos/tencentos support patch

* Thu Nov 09 2023 kianli <kianli@tencent.com> - 4.10.1-9
- Rebuilt for samba 4.18.8

* Thu Oct 12 2023 Miaojun Dong <zoedong@tencent.com> - 4.10.1-8
- Rebuild for curl-8.4.0

* Tue Sep 19 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.10.1-7
- Rebuilt for python 3.11

* Wed Sep 13 2023 kianli <kianli@tencent.com> - 4.10.1-6
- Export PYTHON with python3 macro for python3.11

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.10.1-5
- Rebuilt for OpenCloudOS Stream 23.09

* Wed Jul 19 2023 cunshunxia <cunshunxia@tencent.com> - 4.10.1-4
- Rebuilt for httpd 2.4.57

* Fri Jul 14 2023 Wang Guodong <gordonwwang@tencent.com> - 4.10.1-3
- Rebuilt for sssd 2.9.0

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.10.1-2
- Rebuilt for OpenCloudOS Stream 23.05

* Thu Apr 27 2023 Miaojun Dong <zoedong@tencent.com> - 4.10.1-1
- initial build
